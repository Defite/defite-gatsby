# Defite-gatsby
Personal site built on [Gatsby](https://www.gatsbyjs.org/) ~~with integrated [Netlify CMS](https://github.com/netlify/netlify-cms)~~.

![Personal site built on Gatsby](https://user-images.githubusercontent.com/299118/34795082-e5ea3b58-f661-11e7-8d50-b678f5e77db9.png)


## Running in development
`gatsby develop`

## Running eslint
`npm run eslint`

## Building prod version
`gatsby build`

## Running prod version
`gatsby serve`

PR and issues are welcome!
